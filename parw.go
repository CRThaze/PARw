// parw is a tool which wraps around SSH to easily generate an audit report for the session on
// a host.
package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gopkg.in/yaml.v2"
)

var meta struct {
	TicketURL     string            `yaml:"ticketURL"`
	Ticket        string            `yaml:"ticket"`
	HostsAccessed map[string]string `yaml:"hostsAccessed"`
}

var config struct {
	ReportsDir string `yaml:"reportsDir"`
}

var sessionHostID string

var cleanupTasks []func() = make([]func(), 0)

// deferCleanup registers a function to be run when exiting cleanly.
func deferCleanup(f func()) {
	cleanupTasks = append(cleanupTasks, f)
}

// exitCleanly will iterate over cleanupTasks, running each one in serial, before exiting with the
// specified code.
func exitCleanly(code int) {
	for _, task := range cleanupTasks {
		task()
	}
	os.Exit(code)
}

// check will print an error and call exitCleanly(1) if err is not nil.
func check(err error) {
	if err != nil {
		fmt.Printf("ERROR: %+v", err)
		exitCleanly(1)
	}
}

// fileExists test if a file or directory exists.
func fileExists(f string) bool {
	if _, err := os.Stat(f); err != nil {
		if os.IsNotExist(err) {
			return false
		} else {
			check(err)
		}
	}
	return true
}

// writeReport will commit the report information to the configured directory.
func writeReport(tmpSessionLogFilename string) {
	err := os.MkdirAll(fmt.Sprintf("%s/par-%s", config.ReportsDir, meta.Ticket), 0755)
	check(err)

	err = os.Rename(
		tmpSessionLogFilename,
		fmt.Sprintf("%s/par-%s/%s.log", config.ReportsDir, meta.Ticket, sessionHostID),
	)
	check(err)

	metadata, err := yaml.Marshal(meta)
	check(err)

	ioutil.WriteFile(fmt.Sprintf("%s/par-%s/meta.yaml", config.ReportsDir, meta.Ticket), metadata, 0644)
}

// getMetadata will prompt the user for some metadata information and combine it with any exiting
// metadata file for the given ticket.
func getMetadata() {
	stdinReader := bufio.NewReader(os.Stdin)

	fmt.Print("What is the associated Ticket (or Alert) number?: ")
	ticket, err := stdinReader.ReadString('\n')
	check(err)
	meta.Ticket = strings.TrimSpace(ticket)

	metafilename := fmt.Sprintf("%s/par-%s/meta.yaml", config.ReportsDir, meta.Ticket)
	if fileExists(metafilename) {
		loadYAML(metafilename, &meta)
	} else {
		meta.HostsAccessed = make(map[string]string)
	}

	fmt.Print("What is the Host ID which you just accessed: ")
	id, err := stdinReader.ReadString('\n')
	check(err)
	sessionHostID = strings.TrimSpace(id)

	meta.HostsAccessed[sessionHostID] = fmt.Sprintf("%s.log", sessionHostID)

	if meta.TicketURL == "" {
		fmt.Print("What is the URL for this Ticket?: ")
		url, err := stdinReader.ReadString('\n')
		check(err)
		meta.TicketURL = strings.TrimSpace(url)
	}
}

// loadYAML will open a filename and Unmarshal its contents int a given struct.
func loadYAML(filename string, c interface{}) {
	configFile, err := ioutil.ReadFile(filename)
	check(err)

	err = yaml.Unmarshal(configFile, c)
	check(err)
}

// runSubCommand executes a command with a list of arguments and will tee the STDOUT & STDERR
// output to tmpSessionLog.
func runSubCommand(tmpSessionLog *os.File, commandName string, commandArgs []string) {
	cmd := exec.Command(commandName, commandArgs...)

	logWriter := bufio.NewWriter(tmpSessionLog)
	defer logWriter.Flush()

	cmd.Stdout = io.MultiWriter(os.Stdout, logWriter)
	cmd.Stderr = io.MultiWriter(os.Stderr, logWriter)
	cmd.Stdin = os.Stdin

	if err := cmd.Start(); err != nil {
		fmt.Printf("Failed to run command: %+v", err)
	}

	cmd.Wait()
}

// main
func main() {
	// Parse arguments.
	var commandName string = "ssh"
	var commandArgs []string
	if len(os.Args) > 1 {
		commandArgs = os.Args[1:]
	}

	// Load configuration
	loadYAML(fmt.Sprintf("%s/.parwrc.yaml", os.Getenv("HOME")), &config)

	// Create temp file for logging the session audit log.
	tmpSessionLogFilename := fmt.Sprintf("/tmp/%dparw-sessionlog-tmp", os.Getpid())
	tmpSessionLog, err := os.Create(tmpSessionLogFilename)
	check(err)
	deferCleanup(func() { os.Remove(tmpSessionLogFilename) })

	// Begin SSH session.
	runSubCommand(tmpSessionLog, commandName, commandArgs)

	// Generate report.
	getMetadata()
	writeReport(tmpSessionLogFilename)

	exitCleanly(0)
}

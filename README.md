# PARw: Production Access Report wrapper

## Usage

PARw is a transparent wrapper around your installed SSH command. It will keep a
log of your session and ask you some questions about the ticket or alert ID
associated with it, once you log off the specified host. These will then be used
to create a report for this session under a directory specified in
`$HOME/.parwrc.yaml`.

### Configuration

In order to keep all arguments identical to those of SSH, the location of the
configuration file is hardcoded to `$HOME/.parwrc.yaml`.

It supports the following setting(s):

| Key        | Value Type | Description                                |
| ---------- | ---------- | ------------------------------------------ |
| reportsDir | String     | The directory where reports will be saved. |

## Building

To build simply run:

```bash
make
```

The resulting binary will be found under `bin`.

Using `make`, this project can be built outside of a `GOPATH`, by faking the
`GOPATH` using a symlink under `.gopath`.  However if you would like to use the
`GOPATH` in the traditional way, run:

```bash
GOPATHBUILD=x make
```

## License

X11 (See LICENSE.txt)

## Author

Diego Fernando Carrión (CRThaze)

PACKAGE     = git.xn--e-0fa.net/CRThaze/PARw
MAINSRCFILE = parw.go

ifndef GOPATHBUILD
GOPATH  = $(CURDIR)/.gopath
BASE    = $(GOPATH)/src/$(PACKAGE)
OUTPATH = bin/$(PACKAGE)
else
BASE = .
OUTPATH = $(GOPATH)/bin/$(PACKAGE)
endif


.DEFAULT_GOAL = all


$(BASE):
ifndef GOPATHBUILD
	@mkdir -p $(dir $@)
	@ln -sf $(CURDIR) $@
endif

.PHONY: deps
deps:
	cd $(BASE) && dep ensure

.PHONY: all
all: | $(BASE) deps
	cd $(BASE) && go build -o $(OUTPATH) $(MAINSRC)

.PHONY: clean
clean:
	rm -rf .gopath
	rm -rf bin
	rm -rf vendor
